// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InventorySystem/Item/ItemBase.h"
#include "CraftingComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class MYPROJECT_API UCraftingComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UCraftingComponent();

protected:
public:
    TMap<AItemBase*, TPair<int, TSet<AItemBase*>>> Craft(const TArray<AItemBase*>& Inventory,
                                                         const TArray<TPair<AItemBase*, TSet<AItemBase*>>>&
                                                         IngredientLists) const;

    TMap<AItemBase*, TPair<int, TSet<AItemBase*>>> CalculateCraftList(const TMap<FName, int>& ItemToCounts,
                                                                      const TArray<TPair<AItemBase*, TSet<AItemBase*>>>&
                                                                      IngredientLists) const;
};
