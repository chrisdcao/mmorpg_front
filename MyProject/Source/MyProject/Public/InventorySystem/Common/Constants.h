// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/*
THIS FILE CONSISTS OF:
1. Default values
2. DB Table names
3.
*/

#define CHARACTER_HEALTH_DEFAULT -FLT_MAX
#define CHARACTER_DAMAGE_DEFAULT -FLT_MAX
#define CONSUMABLE_HEAL_DEFAULT -FLT_MAX
#define ITEM_USE_ACTION_TEXT_DEFAULT FText::FromString("No Action Text (Default)")
#define ITEM_DISPLAY_NAME_DEFAULT FText::FromString("No Display Name (Default)")
#define ITEM_NAME_DEFAULT FName("No Name (Default)")
#define ITEM_DESCRIPTION_DEFAULT FText::FromString("No Description (Default)")
#define ITEM_EQUIP_SOCKET_DEFAULT FText::FromString("No Equipping Socket (Default)")
#define ITEM_ESSENCE_DEFAULT FText::FromString("No Essence (Default)")
#define ITEM_EQUIP_STYLE_DEFAULT FText::FromString("No Equip Style (Default)")
