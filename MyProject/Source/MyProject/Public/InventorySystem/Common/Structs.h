﻿#pragma once

#include "Structs.generated.h"

USTRUCT(BlueprintType)
struct FPadding
{
    GENERATED_BODY()
    ;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 Top = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 Right = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 Bottom = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 Left = 0;
};

USTRUCT(BlueprintType)
struct FOverrideExtraSpace
{
    GENERATED_BODY()
    ;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 Horizontal = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 Vertical = 0;
};


// INDEX UTILITIES
USTRUCT(BlueprintType)
struct FIndex2D
{
    GENERATED_BODY()
    ;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 Row;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 Col;

    int32 ConvertTo1DIndex(const int& MaxCol) const;

    bool Uninitialized() const;
};

inline int32 FIndex2D::ConvertTo1DIndex(const int& MaxCol) const
{
    return {Row * MaxCol + Col};
}

inline bool FIndex2D::Uninitialized() const
{
    return Row == -1;
}

inline FIndex2D ConvertIntTo2DIndex(const int& CurrentInt, const int& MaxCol)
{
    return {CurrentInt / MaxCol, CurrentInt % MaxCol};
}
