// Fill out your copyright notice in the Description page of Project Settings.


#include "Crafting System/CraftingComponent.h"

// TODO P1: Add UI Invoke of craft event
// TODO P1: Add UI Update after craft

// Sets default values for this component's properties
UCraftingComponent::UCraftingComponent()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = false;
    PrimaryComponentTick.bStartWithTickEnabled = false;
    // ...
}

TMap<AItemBase*, TPair<int, TSet<AItemBase*>>> UCraftingComponent::Craft(const TArray<AItemBase*>& Inventory,
                                                                         const TArray<TPair<
                                                                             AItemBase*, TSet<AItemBase*>>>&
                                                                         IngredientLists) const
{
    // construct hash map
    TMap<FName, int> ItemToCounts;
    for (const auto& Item : Inventory)
        ItemToCounts[Item->DataName] = Item->Amount;

    // calc craft list
    return CalculateCraftList(ItemToCounts, IngredientLists);
}

TMap<AItemBase*, TPair<int, TSet<AItemBase*>>> UCraftingComponent::CalculateCraftList(
    const TMap<FName, int>& ItemToCounts, const TArray<TPair<AItemBase*, TSet<AItemBase*>>>& IngredientLists) const
{
    TMap<AItemBase*, TPair<int, TSet<AItemBase*>>> CraftList;
    int i = 0;

master_loop:
    i++;
    while (i < IngredientLists.Num())
    {
        int CraftAmount = INT_MAX;
        const auto IngredientList = IngredientLists[i];
        TSet<AItemBase*> IngredientSet = IngredientList.Value;
        const AItemBase* MasterItem = IngredientList.Key;

        // check if we can craft the item, and the amount of item craft-able
        for (const auto& Ingredient : IngredientSet)
        {
            const int RequiredAmount = Ingredient->Amount;
            if (!ItemToCounts.Contains(Ingredient->DataName) || ItemToCounts[Ingredient->DataName] < RequiredAmount)
                goto master_loop;
            CraftAmount = FMath::Min(CraftAmount, FMath::Floor(ItemToCounts[Ingredient->DataName] / RequiredAmount));
        }

        // push the item into Craft list
        // TODO P2: Optimize CraftList (instead of using AItemBase* as key, can we use `Name` or something?
        CraftList[MasterItem] = TPairInitializer(CraftAmount, IngredientSet);
    }

    return CraftList;
}
